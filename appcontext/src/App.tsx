import './App.css';
import { Todo } from './todo/interfaces/Todo';

function App() {
  return (
    <div className="App-header">
      <Todo/>
    </div>
  );
}

export default App;
