import { createContext } from "react";
import { TodoState } from "../interfaces/interfaces";

export type TodoContextrops = {
    todoState: TodoState;
    toggleTodo: ( id: string ) => void;
}



export const TodoContext = createContext<TodoContextrops>({} as TodoContextrops);